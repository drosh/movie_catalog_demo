# Movie Catalog Demo

## Instalation
Python >=3.7 required
```buildoutcfg
pip install -r requirements.txt
```

## Populate empty database
```buildoutcfg
flask populate_db
```

## Start web API
```buildoutcfg
flask run
```

Swagger URI: `/api`