# coding: utf-8
from environs import Env

env = Env()
env.read_env()

ENV = env.str("FLASK_ENV", default="develop")
DEBUG = ENV != "production"

SECRET_KEY = env.str("SECRET_KEY")

SQLALCHEMY_DATABASE_URI = env.str("DATABASE_URL")
SQLALCHEMY_TRACK_MODIFICATIONS = False

DEBUG_TB_ENABLED = DEBUG
DEBUG_TB_INTERCEPT_REDIRECTS = False

API_TITLE = env.str("API_TITLE")
