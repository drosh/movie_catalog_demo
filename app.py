# coding: utf-8
"""Create an application instance."""
import config

from movie_catalog_demo.app import create_app


app = create_app(config)
