from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from .base import Base

metadata = Base.metadata


class Movie(Base):
    __tablename__ = 'movie'

    id = Column(Integer, primary_key=True)
    title = Column(String(256))
    year = Column(Integer)

    cast = relationship('Actor', secondary='actor_movie', lazy='dynamic', backref='movies', uselist=True)
    genres = relationship('Genre', secondary='genre_movie', lazy='dynamic', backref='movies', uselist=True)
