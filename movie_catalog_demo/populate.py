# coding: utf-8
import json
import re

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from data_access_layer import model


def main(connection_query: str, filename: str):

    def validate_name(name: str):
        """Simple regex to filter invalid names by restricted characters"""
        # TODO: improve regex
        return re.match(r"^[^0-9$\[\]()]*$", name)

    engine = create_engine(connection_query, echo=True)
    Session = sessionmaker(engine)

    session = Session()

    with open(filename) as f:
        movies = json.load(f)

    # We want to create actors and genres first to get relations
    actors = []
    genres = []

    for movie in movies:
        actors += movie["cast"]
        genres += movie["genres"]

    actors = set(actors)
    genres = set(genres)

    # We use this map to get sqlalchemy objects by name
    actor_map = {}
    for actor in actors:
        if not validate_name(actor):
            continue
        actor_map[actor] = model.cast.Actor(
            name=actor
        )
        session.add(actor_map[actor])

    # We use this map to get sqlalchemy objects by name
    genre_map = {}
    for genre in genres:
        genre_map[genre] = model.genre.Genre(
            name=genre
        )
        session.add(genre_map[genre])

    session.commit()

    for movie in movies:
        movie_model = model.movie.Movie(
            title=movie['title'],
            year=movie['year'],
            cast=[actor_map[actor] for actor in movie['cast'] if actor_map.get(actor) is not None],
            genres=[genre_map[genre] for genre in movie['genres']]
        )
        session.add(movie_model)

    session.commit()
    session.close()
