# coding: utf-8
from flask_debugtoolbar import DebugToolbarExtension
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Api
from flask_marshmallow import Marshmallow

db = SQLAlchemy()
migrate = Migrate()
debug_toolbar = DebugToolbarExtension()
api = Api()
ma = Marshmallow()
