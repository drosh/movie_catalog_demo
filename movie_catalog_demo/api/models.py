# coding: utf-8
import marshmallow

from data_access_layer import model
from ..extensions import ma


class ActorSchema(ma.SQLAlchemyAutoSchema):

    class Meta:
        model = model.cast.Actor
        load_instance = False
        include_fk = True


class GenreSchema(ma.SQLAlchemyAutoSchema):

    class Meta:
        model = model.genre.Genre
        load_instance = False
        include_fk = True


class MovieSchema(ma.SQLAlchemyAutoSchema):
    cast = ma.Nested(ActorSchema, many=True)
    genres = ma.Nested(GenreSchema, many=True)

    class Meta:
        model = model.movie.Movie
        load_instance = False
        include_fk = True


class ActorStatisticsSchema(marshmallow.Schema):
    name = marshmallow.fields.Str()
    year = marshmallow.fields.Integer()
    movies = marshmallow.fields.Integer()
