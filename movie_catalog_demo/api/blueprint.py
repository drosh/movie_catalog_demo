# coding: utf-8
from flask import Blueprint
from .namespaces.movies import ns as movies_ns
from .namespaces.actors import ns as actors_ns


blueprint = Blueprint(
    "api", __name__, url_prefix="/api"
)
