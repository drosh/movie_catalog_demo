# coding: utf-8
from flask import request
from flask_restplus import Resource, Namespace, fields
from marshmallow.exceptions import ValidationError

from data_access_layer import model
from .. import models as model_ma
from movie_catalog_demo.extensions import api, db
from movie_catalog_demo.utils import get_pagination

movie_schema = model_ma.MovieSchema()
movie_list_schema = model_ma.MovieSchema(many=True)


ns = Namespace('movies')

actor_api_model = ns.model('Actor', {
    'id': fields.Integer
})

genre_api_model = ns.model('Genre', {
    'id': fields.Integer
})

movie_api_model = ns.model('Movie', {
    'title': fields.String(),
    'year': fields.Integer,
    'cast': fields.List(fields.Nested(actor_api_model)),
    'genres': fields.List(fields.Nested(genre_api_model))
})


@ns.route('/<int:id_>')
class Movie(Resource):

    @ns.doc('Get movie')
    def get(self, id_):
        item = db.session.query(model.movie.Movie).get(id_)

        if item is None:
            ns.abort(404)

        return movie_schema.dump(item), 200

    @ns.doc('Update movie')
    @ns.expect(movie_api_model)
    def put(self, id_):
        item = db.session.query(model.movie.Movie).get(id_)

        if item is None:
            ns.abort(404)

        try:
            item_schema = movie_schema.load(request.get_json())
        except ValidationError as e:
            ns.abort(400, str(e))

        if item_schema.get('title') is None or item_schema.get('year') is None:
            ns.abort(400, 'No title or year')

        cast_ids = [cast['id'] for cast in item_schema['cast'] if 'id' in cast]
        cast = db.session.query(model.cast.Actor).filter(model.cast.Actor.id.in_(cast_ids)).all()

        genre_ids = [genre['id'] for genre in item_schema['genres'] if 'id' in genre]
        genres = db.session.query(model.genre.Genre).filter(model.genre.Genre.id.in_(genre_ids)).all()

        item.title = item_schema.get('title')
        item.year = item_schema.get('year')
        item.cast = cast
        item.genres = genres

        db.session.commit()

        return movie_schema.dump(item), 200

    @ns.doc('Delete movie')
    def delete(self, id_):
        item = db.session.query(model.movie.Movie).get(id_)

        if item is None:
            ns.abort(404)

        db.session.delete(item)
        db.session.commit()

        return '', 204


@ns.route('/')
class MovieList(Resource):

    @ns.doc(
        'Get movies',
        params={
            'page': {'in': 'query', 'type': 'int', 'default': 1},
            'per_page': {'in': 'query', 'type': 'int', 'default': 10}
        }
    )
    def get(self):
        page, per_page = get_pagination()
        items = db.session.query(model.movie.Movie).paginate(page, per_page, error_out=False).items
        return movie_list_schema.dump(items), 200

    @ns.doc('Create movie')
    @ns.expect(movie_api_model)
    def post(self):
        try:
            item = movie_schema.load(request.get_json())
        except ValidationError as e:
            ns.abort(400, str(e))

        if item.get('title') is None or item.get('year') is None:
            ns.abort(400, 'No title or year')

        exist = db.session.query(model.movie.Movie).filter_by(title=item.get('title')).first() is not None

        if exist:
            ns.abort(409)

        cast_ids = [cast['id'] for cast in item['cast'] if 'id' in cast]
        cast = db.session.query(model.cast.Actor).filter(model.cast.Actor.id.in_(cast_ids)).all()

        genre_ids = [genre['id'] for genre in item['genres'] if 'id' in genre]
        genres = db.session.query(model.genre.Genre).filter(model.genre.Genre.id.in_(genre_ids)).all()

        item = model.movie.Movie(
            title=item.get('title'),
            year=item.get('year'),
            cast=cast,
            genres=genres
        )
        db.session.add(item)
        db.session.commit()

        return movie_schema.dump(item), 201


api.add_namespace(ns)
