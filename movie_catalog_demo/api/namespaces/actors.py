# coding: utf-8
from flask_restplus import Resource, Namespace
from sqlalchemy import func

from data_access_layer import model
from .. import models as model_ma
from movie_catalog_demo.extensions import api, db
from movie_catalog_demo.utils import get_pagination


actors_statistics_list_schema = model_ma.ActorStatisticsSchema(many=True)


ns = Namespace('actors')


@ns.route('/')
class ActorStatistics(Resource):

    @ns.doc(
        'Get statistics',
        params={
            'page': {'in': 'query', 'type': 'int', 'default': 1},
            'per_page': {'in': 'query', 'type': 'int', 'default': 10}
        }
    )
    def get(self):
        page, per_page = get_pagination()

        items = db.session\
            .query(model.cast.Actor.name, model.movie.Movie.year, func.count(model.movie.Movie.id).label('movies'))\
            .join(model.cast.Actor.movies) \
            .group_by(model.cast.Actor.name) \
            .group_by(model.movie.Movie.year)\
            .order_by(model.cast.Actor.name)\
            .order_by(model.movie.Movie.year)\
            .paginate(page, per_page, error_out=False).items

        # TODO refactor to use ma.SQLAlchemyAutoSchema
        items = [dict(name=i.name, year=i.year, movies=i.movies) for i in items]

        return actors_statistics_list_schema.load(items), 200


api.add_namespace(ns)
