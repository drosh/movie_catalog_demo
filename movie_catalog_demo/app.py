# coding: utf-8
from flask import Flask

from . import extensions, commands
from . import api


def create_app(config):
    """Create application factory.
    :param config: The configuration object to use.
    """
    app = Flask(__name__.split(".")[0])
    app.config.from_object(config)
    register_extensions(app)
    register_blueprints(app)
    register_commands(app)
    return app


def register_extensions(app):
    """Register Flask extensions."""
    extensions.db.init_app(app)
    extensions.debug_toolbar.init_app(app)
    extensions.migrate.init_app(app, extensions.db)
    extensions.api.init_app(api.blueprint.blueprint, title=app.config.get('API_TITLE'))
    extensions.ma.init_app(app)
    return None


def register_blueprints(app):
    """Register Flask blueprints."""
    app.register_blueprint(api.blueprint.blueprint)
    return None


def register_commands(app):
    """Register Click commands."""
    app.cli.add_command(commands.populate_db)

