# coding: utf-8
from flask import request, abort


def get_pagination():
    try:
        page = int(request.args.get('page', 1))
        per_page = int(request.args.get('per_page', 10))
    except ValueError:
        page = 1
        per_page = 10

    if not page >= 1 or not per_page > 0:
        abort(400)

    return page, per_page
