# coding: utf-8
import click


@click.command("populate_db")
def populate_db():
    """Populate database"""
    from movie_catalog_demo.populate import main
    main('sqlite:///data_access_layer/db', 'movies.json')
